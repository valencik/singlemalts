ThisBuild / scalaVersion := "2.13.5"

val catsVersion            = "2.5.0"
val catsEffectVersion      = "2.3.1"
val fs2Version             = "2.5.0"
val scalaScrapeVersion     = "2.2.0"
val http4sVersion          = "0.21.22"
val circeVersion           = "0.13.0"
val munitVersion           = "0.7.23"
val munitCatsEffectVersion = "1.0.1"

lazy val commonSettings = Seq(
  organization := "io.pig",
  version := "0.1.0-SNAPSHOT",
  libraryDependencies ++= Seq(
    "org.typelevel" %% "cats-core"           % catsVersion,
    "org.typelevel" %% "cats-effect"         % catsEffectVersion,
    "co.fs2"        %% "fs2-core"            % fs2Version,
    "co.fs2"        %% "fs2-io"              % fs2Version,
    "org.http4s"    %% "http4s-blaze-client" % http4sVersion,
    "io.circe"      %% "circe-core"          % circeVersion,
    "io.circe"      %% "circe-generic"       % circeVersion,
    "io.circe"      %% "circe-fs2"           % circeVersion,
    "org.scalameta" %% "munit"               % munitVersion           % Test,
    "org.typelevel" %% "munit-cats-effect-2" % munitCatsEffectVersion % Test
  ),
  testFrameworks += new TestFramework("munit.Framework")
  //scalacOptions in Compile ~= filterConsoleScalacOptions
)

lazy val root = (project in file("."))
  .aggregate(domain, scrape, storage, app)

lazy val domain = (project in file("modules/10-domain"))
  .settings(commonSettings)
  .settings(
    name := "singlemalts-domain"
  )

lazy val data = (project in file("modules/10-data"))
  .settings(commonSettings)
  .settings(
    name := "singlemalts-data"
  )

lazy val storage = (project in file("modules/20-storage/"))
  .dependsOn(domain)
  .settings(commonSettings)
  .settings(
    name := "singlemalts-storage"
  )

lazy val scrape = (project in file("modules/20-scrape"))
  .dependsOn(domain)
  .settings(commonSettings)
  .settings(
    name := "singlemalts-scrape",
    libraryDependencies ++= Seq(
      "net.ruippeixotog" %% "scala-scraper" % scalaScrapeVersion
    ),
    console / initialCommands := """
      |import net.ruippeixotog.scalascraper.browser.JsoupBrowser
      |import net.ruippeixotog.scalascraper.dsl.DSL._
      |import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
      |import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
      |
      |val browser = JsoupBrowser()
      |val doc = browser.parseFile("liquer.html")
      """.stripMargin
  )

lazy val app = (project in file("modules/30-app"))
  .dependsOn(domain, data, scrape, storage)
  .settings(commonSettings)
  .settings(
    name := "singlemalts-app"
  )
