addSbtPlugin("io.github.davidgregory084" % "sbt-tpolecat" % "0.1.17")
addSbtPlugin("org.scalameta"             % "sbt-scalafmt" % "2.4.2")
addSbtPlugin("ch.epfl.scala"             % "sbt-bloop"    % "1.4.8")
