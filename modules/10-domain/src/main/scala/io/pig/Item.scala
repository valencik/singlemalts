package io.pig.singlemalts

case class Item(name: String, link: String, price: String)
