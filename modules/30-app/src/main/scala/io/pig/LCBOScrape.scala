package io.pig.singlemalts

import cats.effect._
import cats.syntax.all._
import org.http4s.client.Client
import org.http4s.client.blaze._
import org.http4s.Uri
import fs2.Stream
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.global
import data.SiteData

object LCBOScrape extends IOApp {

  def pagesStream(client: Client[IO])(initialUri: Uri): Stream[IO, SearchPageLCBO] =
    Stream.unfoldLoopEval[IO, Uri, SearchPageLCBO](initialUri) { uri =>
      Scrape.scrapeOnce(client)(uri).fproduct(_.nextUri)
    }

  def serializePage(filename: String, page: Stream[IO, SearchPageLCBO]): Stream[IO, Unit] =
    page
      .flatMap(p => Stream.emits(p.items))
      .through(ItemStore.serialize(filename))

  def scrapeAndSerialize(
      client: Client[IO]
  )(initialUri: Uri, filename: String): Stream[IO, SearchPageLCBO] =
    pagesStream(client)(initialUri)
      .observe(s => serializePage(filename, s))
      .onFinalize(IO { println("Finished scraping and serializing items!") })

  def scrape(client: Client[IO])(initialUri: Uri, filename: String): Stream[IO, SearchPageLCBO] =
    (Stream(()) ++ Stream.fixedDelay[IO](4.seconds))
      .zipRight(scrapeAndSerialize(client)(initialUri, filename))

  def run(args: List[String]): IO[ExitCode] = {
    val startUri   = Uri.unsafeFromString(SiteData.gin)
    val outputFile = "outputs/gin.jsonl"
    BlazeClientBuilder[IO](global).resource.use { client =>
      scrape(client)(startUri, outputFile).compile.drain
        .as(ExitCode.Success)
    }
  }
}
