package io.pig.singlemalts

import cats.effect._
import cats.syntax.all._
import fs2.Stream

object LocalOnly extends IOApp {

  def converter: Stream[IO, Unit] = {
    val page = ItemStore
      .localData("data/test/lcbo/lcbo-scotch-first-page.html")

    Stream
      .eval(page)
      .map(Scrape.parseHtml)
      .mapFilter(d => SearchPageLCBO.fromDocument(d))
      .flatMap(p => Stream.emits(p.items))
      .through(ItemStore.serialize("scotch.jsonl"))
      .onFinalize(IO { println("serialized items to disk") })
  }

  def run(args: List[String]): IO[ExitCode] =
    converter.compile.drain.as(ExitCode.Success)
}
