package io.pig.singlemalts
package data

object SiteData {
    val scotchSingleMalt =
      "https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/spirits-15/whisky-whiskey-15020/scotch-single-malts-15020059"
    val gin =
      "https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/spirits-15/gin-15014"
}
