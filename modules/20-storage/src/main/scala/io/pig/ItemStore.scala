package io.pig.singlemalts

import cats.effect._
import fs2.{io => fs2io, text, Stream, Pipe}
import io.circe._, io.circe.generic.semiauto._
//import io.circe.fs2._
import java.nio.file.Paths
import java.nio.file.StandardOpenOption

object ItemStore {
  implicit val fooDecoder: Decoder[Item] = deriveDecoder[Item]
  implicit val fooEncoder: Encoder[Item] = deriveEncoder[Item]

  private final def encoder[F[_], A](implicit encode: Encoder[A]): Pipe[F, A, String] =
    _.map { a => encode(a).noSpacesSortKeys }

  def localData(filename: String)(implicit shift: ContextShift[IO]): IO[String] =
    Stream
      .resource(Blocker[IO])
      .flatMap { blocker =>
        fs2io.file
          .readAll[IO](Paths.get(filename), blocker, 4096)
          .through(text.utf8Decode)
      }
      .compile
      .foldMonoid

  def serialize(
      filepath: String
  )(items: Stream[IO, Item])(implicit shift: ContextShift[IO]): Stream[IO, Unit] =
    Stream.resource(Blocker[IO]).flatMap { blocker =>
      val flags = List(StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
      items
        .through(encoder)
        .intersperse("\n")
        .through(text.utf8Encode)
        .through(fs2io.file.writeAll(Paths.get(filepath), blocker, flags = flags))
    }
}
