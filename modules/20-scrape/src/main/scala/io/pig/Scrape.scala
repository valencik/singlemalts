package io.pig.singlemalts

import cats.effect._
import cats.syntax.all._
import cats.arrow.Arrow

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.model.Document
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import org.http4s.client._
import org.http4s.Uri

case class SearchPageLCBO(
    items: List[Item],
    numResults: Int,
    nextUri: Option[Uri]
) {}
object SearchPageLCBO {
  def fromDocument(doc: Document): Option[SearchPageLCBO] = {
    val items = parseItems(doc)
    numResultsAndPagination(doc).map { case (num, page) =>
      SearchPageLCBO(items, num, Option.when(shouldPaginate(num, page))(page))
    }
  }

  def parseItems(doc: Document): List[Item] =
    for {
      product <- doc >> elementList("div.product_info")
      name    <- product >> texts("a[href]")
      link    <- product.select("a[href]").map(_.attr("href"))
      price   <- product >> texts("span.price")
    } yield Item(name, link, price)

  def numResults(doc: Document): Option[Int] = {
    val css = "span.search_results_total > span.spaced-character"
    (doc >> text(css)).toIntOption
  }

  def paginationUrl(doc: Document): Option[Uri] = {
    val css = "#WC_SearchBasedNavigationResults_pagination_link_right_categoryResults"
    val url = doc >> attr("href")(css)
    Uri.fromString(url).toOption
  }

  def shouldPaginate(numResults: Int, url: Uri): Boolean =
    url.params.get("beginIndex").exists(_.toIntOption.exists(_ <= numResults))

  def combine[F[_, _]: Arrow, A, B, C](fab: F[A, B], fac: F[A, C]): F[A, (B, C)] =
    Arrow[F].lift((a: A) => (a, a)) andThen (fab split fac)

  val numResultsAndPagination: Document => Option[(Int, Uri)] = {
    combine(numResults(_), paginationUrl(_)).andThen(_.bisequence)
  }
}

object Scrape {

  def fetchPage(client: Client[IO])(url: Uri): IO[String] =
    IO(println(s"fetching ${url.show}...")) *> client.expect[String](url)

  def parseHtml(html: String): Document =
    JsoupBrowser().parseString(html)

  def scrapeOnce(client: Client[IO])(url: Uri): IO[SearchPageLCBO] =
    for {
      html <- fetchPage(client)(url)
      doc = parseHtml(html)
      page <- IO.fromOption(SearchPageLCBO.fromDocument(doc))(???)
    } yield page

}
