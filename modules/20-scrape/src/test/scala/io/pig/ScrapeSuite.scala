package io.pig.singlemalts

import org.http4s.implicits._
import cats.effect.IO
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import munit.CatsEffectSuite

class SearchPageLCBOSuite extends CatsEffectSuite {
  import SearchPageLCBO._

  val totalResults = 153
  val scotchFirstPage =
    uri"https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/spirits-15/whisky-whiskey-15020/scotch-single-malts-15020059?pageView=grid&orderBy=5&fromPage=catalogEntryList&beginIndex=0"
  val scotchSecondPage =
    uri"https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/spirits-15/whisky-whiskey-15020/scotch-single-malts-15020059?pageView=grid&orderBy=5&fromPage=catalogEntryList&beginIndex=12"
  val scotchLastPage =
    uri"https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/spirits-15/whisky-whiskey-15020/scotch-single-malts-15020059?pageView=grid&orderBy=5&fromPage=catalogEntryList&beginIndex=156"

  val firstPageDoc = IO(JsoupBrowser().parseFile("data/test/lcbo/lcbo-scotch-first-page.html"))

  test("fromDocument should yield SearchPageLCBO with correct numResults") {
    val obtained = firstPageDoc.map(fromDocument).map(_.get)
    obtained.map(_.numResults).assertEquals(totalResults)
  }

  test("fromDocument should yield SearchPageLCBO with pagination Uri") {
    val obtained = firstPageDoc.map(fromDocument).map(_.get)
    obtained.map(_.nextUri).assertEquals(Some(scotchSecondPage))
  }

  test("fromDocument should yield SearchPageLCBO with list of Items") {
    val obtained = firstPageDoc.map(fromDocument).map(_.get)
    obtained.map(_.items.length).assertEquals(12)
  }

  test("should paginate when beginIndex is below number of results") {
    val firstPagination  = shouldPaginate(153, scotchFirstPage)
    val secondPagination = shouldPaginate(153, scotchSecondPage)
    assertEquals(firstPagination, true)
    assertEquals(secondPagination, true)
  }

  test("should not paginate when beginIndex is below number of results") {
    val obtained = shouldPaginate(153, scotchLastPage)
    assertEquals(obtained, false)
  }

  test("should extract items from scotch page") {
    val itemsIO = firstPageDoc.map(parseItems)
    val expectedFirstTwo = List(
      Item(
        name = "The Glenlivet Archive 21 Year Old Scotch Whisky",
        link =
          "https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/scotch-single-malts-15020059/the-glenlivet-archive-21-year-old-scotch-whisky-12856",
        price = "$299.95"
      ),
      Item(
        name = "The Glenlivet 12 Year Old Single Malt Scotch Whisky",
        link =
          "https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/scotch-single-malts-15020059/the-glenlivet-12-year-old-single-malt-scotch-whisky-21097",
        price = "$59.95"
      )
    )
    itemsIO.map(items => items.take(2)).assertEquals(expectedFirstTwo) *>
      itemsIO.map(items => items.length).assertEquals(12)
  }

  test("should extract pagination url") {
    val urlIO = firstPageDoc.map(paginationUrl)
    urlIO.map(_.contains(scotchSecondPage)).assertEquals(true)
  }
}
